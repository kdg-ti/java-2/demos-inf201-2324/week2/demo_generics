package be.kdg.java2;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
//        List<Student> studentList = new ArrayList<>();
//        List studentListNoGenerics = new ArrayList();
//        studentList.add(new Student("Joske"));
//        //studentList.add("Bananen"); --> gaat niet!
//        for (Student s: studentList) {
//            System.out.println(s.getName());
//        }
//        studentListNoGenerics.add(new Student("Jefke"));
//        studentListNoGenerics.add("Bananen");
//        for (Object s: studentListNoGenerics) {
//            System.out.println(((Student)s).getName());
//        }
//        Box<Student> studentBox = new Box<>(new Student("Marieke"));
//        studentBox.setContents(new Student("Anneke"));
//        System.out.println(studentBox.getContents().getName());
        //Box<String> stringBox = new Box<>("Bananen");
        //OldBox studentBox2 = new OldBox("appels");

        //Generics en wildcards:
        List<Person> personList = new ArrayList<>();
        List<Student> studentList = new ArrayList<>();
        //personList = studentList;//mag niet, ik zou op deze manier docenten in de studentList kunnen stoppen!
        personList.add(new Docent());
        //studentList = personList;// mag niet, personList zou docenten kunnen bevatten!
        werkMetPersonList(personList);
        //werkMetPersonList(studentList);//mag niet, zelfde reden als hierboven (in methode zou je docenten in studentList kunnen stoppen!)
        werkMetPersonListMetWildCards(studentList);
    }

    public static void werkMetPersonList(List<Person> personList){
        for (Person p: personList) {
            System.out.println(p);
        }
    }

    public static void werkMetPersonListMetWildCards(List<? extends Person> personList){
        for (Person p: personList) {
            System.out.println(p);
        }
        //personList.add(new Person()); --> adden lukt dan wel niet...!
    }
}
