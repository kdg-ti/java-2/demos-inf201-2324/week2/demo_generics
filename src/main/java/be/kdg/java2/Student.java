package be.kdg.java2;

/**
 * Deze klasse beschrijft een standaard Student aan de KdG hogeschool
 *
 * @author vochtenh
 * @version 1.0
 */
public class Student extends Person{
    private String name;

    public Student(String name) {
        this.name = name;
    }

    /**
     * Deze methode is een heel speciale getter method, amai!
     * @return geeft de naam van de student terug
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }
}
