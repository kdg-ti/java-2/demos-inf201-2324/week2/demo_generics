package be.kdg.java2;

public class OldBox {
    private Object contents;

    public OldBox(Object contents) {
        this.contents = contents;
    }

    public Object getContents() {
        return contents;
    }

    public void setContents(Object contents) {
        this.contents = contents;
    }
}
